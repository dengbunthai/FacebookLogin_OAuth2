package com.example.demo.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.util.HashMap;


/**
 * Created by msi on 31/Dec/2017.
 */
@SpringBootApplication
@EnableOAuth2Sso
@RestController
public class SocialApplication extends WebSecurityConfigurerAdapter {

    @RequestMapping("/user")
    public Principal user(Principal principal) {

        Authentication a = SecurityContextHolder.getContext().getAuthentication();
        HashMap data = (HashMap) ((OAuth2Authentication) a).getUserAuthentication().getDetails();


        String facebookId = data.get("id").toString();
        String name = data.get("name").toString();
        String email = data.get("email").toString();

        System.out.println(facebookId + ", " + name + ", " + email);

        return principal;
    }

    /*  If you want your page redirect to another page after login with Facebook Success */
//    @RequestMapping("/user")
//    public void user(Principal principal, HttpServletResponse response) {
//
//        Authentication a = SecurityContextHolder.getContext().getAuthentication();
//        HashMap data = (HashMap) ((OAuth2Authentication) a).getUserAuthentication().getDetails();
//
//
//        String facebookId = data.get("id").toString();
//        String name = data.get("name").toString();
//        String email = data.get("email").toString();
//
//        System.out.println(facebookId + ", " + name + ", " + email);
//
//        try {
//            response.sendRedirect("example.html");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }

    public static void main(String[] args) {
        SpringApplication.run(SocialApplication.class, args);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic()
                .and()
                .csrf().disable()
                .logout()
                .logoutSuccessUrl("/")
        ;
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/**");
        web.ignoring().antMatchers("/static/**");
    }
}