package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by msi on 15/Jan/2018.
 */
@Controller
public class WebController {

    @RequestMapping({"/", "/index"})
    public String index(){
        return "index";
    }

}
